import { createTheme } from "@mui/material";

export const appTheme  = createTheme({
    components: {
      MuiContainer: {
        styleOverrides: {
          root: {
            font: 24
          }
        }
      }
    }
  });