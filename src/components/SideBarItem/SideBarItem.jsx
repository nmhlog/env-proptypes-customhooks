import React from "react";
import {
  ListItemButton,
  ListItemAvatar,
  Avatar,
  ListItemText,
  Stack,
} from "@mui/material";
import propTypes from 'prop-types';

function SideBarItem(props) {
  const { text1, text2, use } = props;
  return (
    <>
      <ListItemButton>
        <ListItemAvatar>
          <Avatar>{props.children}</Avatar>
        </ListItemAvatar>

        <Stack direction={use?"column" : "row"}>
          <ListItemText primary={text1} sx={{color:"red"}} />
          {use ? <ListItemText secondary={text2}/> : null}
        </Stack>
      </ListItemButton>
    </>
  );
}

SideBarItem.propTypes={
  text1:propTypes.string,
  text2:propTypes.string,
  use:propTypes.bool,
  children:propTypes.object,
}
export default SideBarItem;