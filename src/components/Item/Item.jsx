import React from "react";
import {
  Card,
  CardHeader,
  CardMedia,
  Avatar,
  CardContent,
  Typography
} from "@mui/material";
import { red } from "@mui/material/colors";
import propTypes from "prop-types";

function Item(props) {
  const {author, title, img, datePost, description} = props;
  return (
    <>
      <Card sx={{ maxHeight:"350px",maxWidth:"300px", minHeight:"300px", minWidth:"200px" }}>
        <CardHeader
          avatar={
            <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
              {author[0]}
            </Avatar>
          }
          title={title}
          subheader={datePost}
        />
        <CardMedia component="img" height="200px" image={img} alt="title" />
        <CardContent sx={{ whiteSpace: 'normal',overflow:"none" }}>
          <Typography variant="body2" color="black">
            {description}
          </Typography>
          
        </CardContent>
      </Card>
    </>
  );
}

Item.propTypes = {
  author:propTypes.string, 
  title:propTypes.string, 
  img:propTypes.string, 
  datePost:propTypes.string, 
  description:propTypes.string
};
export default Item;