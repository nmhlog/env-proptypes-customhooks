import { AppBar, Toolbar, Button, Stack } from "@mui/material";
import CameraAltIcon from '@mui/icons-material/CameraAlt';
import MenuIcon from '@mui/icons-material/Menu';
import React from "react";
import propTypes from "prop-types";

function NavBar(props) {
  const { use } = props;
  return (
    <AppBar position="relative" sx={{ boxShadow: "none" }}>
      <Toolbar
        sx={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Stack direction={"row"} spacing={2}>
          <CameraAltIcon sx={{ mx: "auto", mt: "2%" }} />
          {use ? (
            <>
              <Button variant="text" sx={{ color: "red" }}>
                Blog App
              </Button>
              <Button variant="text" sx={{ color: "Blue" }}>
                About
              </Button>
              <Button variant="text" sx={{ color: "Blue" }}>
                History
              </Button>
            </>
          ) : null}
        </Stack>
        {use ? (
          <Button variant="text" sx={{ color: "white" }}>
            User
          </Button>
        ) : (
          <MenuIcon
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          />
        )}
      </Toolbar>
    </AppBar>
  );
}
NavBar.propTypes={
  use:propTypes.bool,
}
export default NavBar;