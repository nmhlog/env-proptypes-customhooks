import axios from "axios";
import { useEffect,useState } from "react"

const useAsync=(url)=>{
    const [data,setData] = useState([]);


    useEffect(()=>{
        const apiGet = async()=>{
            try{
                const response = await axios.get(url);
                let resData = response.data;
                try {
                resData = JSON.parse(resData);
                } catch (err) {
                console.log(err);
                resData = resData.replace(/""/gm, '"');
                } finally {
                setData(JSON.parse(resData));
                }
            } catch(e){
                console.error(e);
            }
        }
        apiGet();
    },[])

    return {data};

}
export default useAsync;