// import logo from './logo.svg';
// import './App.css';
import React from "react";
import useMediaQuery from "@mui/material/useMediaQuery";
import Grid from "@mui/material/Grid";
import { Box, Stack, createTheme, ThemeProvider } from "@mui/material";
import NavBar from "./components/NavBar/NavBar";
import Item from "./components/Item/Item";
import { List } from "@mui/material";
import PhotoSizeSelectActualIcon from "@mui/icons-material/PhotoSizeSelectActual";
import WorkIcon from "@mui/icons-material/Work";
import BeachAccessRoundedIcon from "@mui/icons-material/BeachAccessRounded";
import SideBarItem from "./components/SideBarItem/SideBarItem";
import useAsync from "./hooks/useAsync";


function App() {
  const {data}= useAsync(`${process.env.REACT_APP_BASE_URL}`);
  const use = useMediaQuery("(min-width:900px)");
  const theme = createTheme({
    typography: {
      fontFamily: ["Montserrat", "sans-serif"].join(","),
    },
    palette: {
      primary: {
        main: process.env.REACT_APP_COLOR_PRIMARY,
      },
    },
  });

  

  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <NavBar use={use} />
        <Grid
          container
          spacing={0}
          sx={{
            maxWidth: "1200px",
            mx: "auto",
          }}
        >
          <Grid item md={9} xs={12}>
            <Stack
              direction={use ? "row" : "column"}
              spacing={2}
              justifyContent={"center"}
              alignItems={"center"}
              flexWrap={"wrap"}
              useFlexGap
              sx={{ mx: "auto" }}
            >
              {data.map((data, id) => (
                <Item
                  key={id}
                  author={data.author}
                  title={data.title}
                  img={data.img}
                  datePost={data.datePost}
                  description={data.description}
                />
              ))}
            </Stack>
          </Grid>
          <Grid item md={3} xs={12}>
            <Box
              sx={{
                height: use ? "100%" : "100px",
              }}
            >
              <List
                sx={{
                  width: "100%",
                  maxWidth: use ? 360 : 170,
                  bgcolor: "background.paper",
                  position: use ? "fixed" : null,
                }}
              >
                <SideBarItem use={use} text1={"Photo"} text2={"Jul, 04 2023"}>
                  <PhotoSizeSelectActualIcon />
                </SideBarItem>
                <SideBarItem use={use} text1={"Work"} text2={"Jul, 04 2023"}>
                  <WorkIcon />
                </SideBarItem>
                <SideBarItem
                  use={use}
                  text1={"Vacation"}
                  text2={"Jul, 04 2023"}
                >
                  <BeachAccessRoundedIcon />
                </SideBarItem>
              </List>
            </Box>
          </Grid>
        </Grid>
      </ThemeProvider>
    </div>
  );
}

export default App;
